function Request() {

  this.send = function send(url) {
    return new Promise((resolve, reject) => {
      const rq = new XMLHttpRequest();
      rq.open('GET', url);
      rq.onload = () => rq.status == 200 ? resolve(rq.response) : reject(err);
      rq.send();
    });
  }
}

module.exports.Request = Request;