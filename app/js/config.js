function Config() {

  const api_key = '579680975c0a476a8906708b0cc45739';

  /*
  // URL Settings:
  // base + path + api_key + params
  */
  this.urls = {
    base: 'https://api.themoviedb.org/3',
    baseImgPhone: 'https://image.tmdb.org/t/p/w300',
    baseImgTablet: 'https://image.tmdb.org/t/p/w780',
    baseImgDesktop: 'https://image.tmdb.org/t/p/w1280'
  }
  
  this.param = {
    lang: '&language=',
    query: '&query=',
    page: '&page=',
    adult: '&include_adult=',
    key: '?api_key=',
  }

  this.paths = {
    upcoming: `/movie/upcoming${this.param.key + api_key}`,
    toprated: `/movie/top_rated${this.param.key + api_key}`,
    popular: `/movie/popular${this.param.key + api_key}`,
    nowplaying: `/movie/now_playing${this.param.key + api_key}`,
    latest: `/movie/latest${this.param.key + api_key}`,
    search: `/search/movie${this.param.key + api_key}`
  }

  // Availiable Languages
  this.lang = {
    eng: 'en-US',
    ge: 'de-DE'
  }

}

module.exports.Config = Config;