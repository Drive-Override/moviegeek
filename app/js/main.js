const Handlebars = require('handlebars');
const { Config } = require('./config');
const { Request } = require('./request');

(() => {

  const config = new Config();
  const request = new Request();

  function init() {
    
    // Get Data for Start Page!
    const url = config.urls.base + config.paths.popular;

    // Get Most Popular Movies
    request.send(url).then(response => {
      const data = JSON.parse(response);
      console.log(data);
      buildTemplate('slider_tpl', data);
      // init slider script
      slider();
    }).catch(err => {
      console.log('Promise Failed', err);
    });

  }

  function buildTemplate(template, obj) {
    // template has to be an unique id that includes the handlebar template
    const tpl = document.getElementById(template).innerHTML;

    // Compile Template
    const renderer = Handlebars.compile(tpl);

    // Render
    const result = renderer(obj);

    // Put Back Into Dom
    document.getElementById('slider-wrapper').innerHTML = result;
  }

  function slider() {
    const slides = document.querySelectorAll('.slide');
    const slider = document.getElementById('slider');
    let activeLink = 0;

    for (let i = 0; i < slides.length; i++) {
      let slide = slides[i];
    }

    slides[activeLink].classList.add('active');

    function next() {

      if (activeLink >= slides.length -1) {
        slides[activeLink].classList.remove('active');
        activeLink = 0;
      } else {
        slides[activeLink].classList.remove('active');
        activeLink = activeLink + 1;
      }
      
      slides[activeLink].classList.add('active');
    }
    
    setInterval(() => {
      next();
    }, 4500);
  }

  // Load Initializing Scripts
  window.addEventListener('load', init);

})()