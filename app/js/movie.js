const { Request } = require('./request');

function Movie() {
  // Aviabiable URL Marks
  /*
  /search
  /popular
  /movie/{movie_id}
  /movie/{movie_id}/credits
  /movie/{movie_id}/images
  /movie/{movie_id}/release_dates
  /movie/{movie_id}/keywords
  /movie/{movie_id}/videos
  /movie/{movie_id}/reviews
  /movie/latest
  /movie/now_playing
  /movie/popular
  /movie/top_rated
  /movie/upcoming
  */

  // Logic
  /*
  movie_id required -> fetch film title before, click on movie -> get id
  */

  // locals
  let baseURL = 'https://api.themoviedb.org/3';
  const key = '579680975c0a476a8906708b0cc45739';
  const request = new Request();

  // Build request URL
  this.buildURL = function buildURL(options) {
    let option = options || {};
    let query = options.query || '';
    let year = options.year || '';
    let action = options.action || undefined;
    let api_key = `?api_key=${key}`;
    let page = options.page || '';
    let id = options.id || undefined;

    let paramQuery;

    if (query) {
      paramQuery = `&query=${query.replace(/\s/g, '%20')}`;
    }

    if (year) {
      paramYear = `&year=${year}`
    }

    if (page) {
      paramPage = `&page=${page}`;
    }

    switch(action) {
      case 'search': return baseURL + '/search' + api_key + paramQuery + paramYear;
      break;
      case 'popular': return baseURL + '/movie/popular' + api_key + paramPage;
      break;
      case 'latest': return baseURL + '/movie/latest' + api_key + paramPage;
      break;
      case 'topRated': return baseURL + '/movie/top_rated' + api_key + paramPage;
      break;
      case 'upcoming': return baseURL + '/movie/upcoming' + api_key + paramPage;
      break;
      case 'nowPlaying': return baseURL + '/movie/now_playing' + api_key + paramPage;
      break;
      case 'credits': return baseURL + `/movie/${id}/credits` + api_key;
      break;
    }
  }

  this.get = function get(type) {
    const mostPopURL = this.buildURL({action: 'popular', page: 1});
    const topRatedURL = this.buildURL({action: 'topRated', page: 1});
    const latestURL = this.buildURL({action: 'latest', page: 1});
    const nowURL = this.buildURL({action: 'nowPlaying', page: 1});
    const upcomingURL = this.buildURL({action: 'upcoming', page: 1});
    // Temp Hard Coded
    const creditURL = this.buildURL({action: 'credits', page: 1, id: 284053}); 
    let result;

    function fetch(url, type) {
      request.send(url).then(response => {
        result = response;
        // Save Movie Type List on Movie Obj
        window.Movie[type] = JSON.parse(result);
      }).catch(err => {
        console.log('Request Error', err);
      });
    }
    
    switch(type) {

      case 'mostPopular': (() => {
        fetch(mostPopURL, type);
      })();
      break;

      case 'topRated': (() => {
        fetch(topRatedURL, type);
      })();
      break;

      case 'latest': (() => {
        fetch(latestURL, type);
      })();
      break;

      case 'upcoming': (() => {
        fetch(upcomingURL, type);
      })();
      break;

      case 'nowPlaying': (() => {
        fetch(nowURL, type);
      })();
      break;

      // movie_id REQUIRED!
      case 'credits': (() => {
        fetch(creditURL, type);
      })();
      break;

    }
  }

}

module.exports.Movie = Movie;